// console.log("Hello World!")

// [SECTION] Parameters and Arguments
// Functions in JS are lines/blocks of code that tells our device or application to perform a certain task

// Last session, we learned how to use a basic function

function printInput() {
	let nickname = prompt("Enter your nickname")

	console.log("Hi, " + nickname)
}

printInput()

// In some cases, a basic function may not be ideal
// For other cases, functions can also process data directly into it instead

// Parameter is located inside the "(parameter_name)" after the function name.
// Parameter names are just names to refer to the argument
function printName(name) {
	console.log("My Name is " + name)
}

// Argument is located in the invocation of the function. The data then will be passed on the parameter which can be used inside the code blocks of a particular function.
// The order of the arguments follow the same order of the parameters.
printName("Juana")
printName("John")
printName("Jane")

// variables can also be passed as an argument
let sampleVariable = "Yul"

printName(sampleVariable)

// function arguments cannot be used by a function if there are no parameters provided

function checkDivisibilityBy8(number) {
	let remainder = number % 8;

	console.log("The remainder of " + number + " divided by 8 is " + remainder)

	let isDivisibleBy8 = remainder === 0;

	console.log("Is " + number + " divisible by 8?")
	console.log(isDivisibleBy8)
}

checkDivisibilityBy8(64)
checkDivisibilityBy8(28)

// Function as Arguments

function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed")
}

function invokeFunction(argumentFunction) {
	argumentFunction()
}

invokeFunction(argumentFunction)

// finding more info about a function, we can use console.log(function_name)
console.log(argumentFunction)

// Function with Multiple Parameters

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName)
}

// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
// "Cruz" will be stored in the parameter "lastName"
createFullName("Juan", "Dela", "Cruz")

// the parameter "lastName" will return an undefined value
createFullName("Juan", "Dela")

// the argument "Hello" will be disregarded since the function is expecting 3 arguments only
createFullName("Juan", "Dela", "Cruz", "Hello")

let firstName = "John"
let middleName = "Doe"
let lastName = "Smith"

createFullName(firstName, middleName, lastName)

function printFullName(middleName, firstName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName)
}

printFullName("Juan", "Dela", "Cruz")

// Return Statement

function returnFullName(firstName, middleName, lastName) {
	console.log("test console message")

	return firstName + " " + middleName + " " + lastName

	// codeblocks after the return statement will not be executed inside the function
	console.log("this message will not be printed")
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos")
console.log(completeName)

// returnFullName("Jeffrey", "Smith", "Bezos") -> it will not work if our function is expecting a return value because returned values should be stored in a variable

console.log(returnFullName(firstName, middleName, lastName))

// You can also create a variable inside the function to contain the result and return the variable insted

function returnAddress(city, country) {
	let fullAddress = city + ", " + country
	return fullAddress
}

let myAddress = returnAddress("Cebu City", "Philippines")
console.log(myAddress);

// On the other hand, when a function only has console.log() to display its result will return indefined instead

function printPlayerInfo(username, level, job) {
	console.log("Username: " + username)
	console.log("Level: " + level)
	console.log("Job: " + job)
}

// this codeblock returns undefined because printPlayerInfo returns nothing
let user1 = printPlayerInfo("knight_white", 95, "paladin")
console.log(user1)
